import 'package:flutter/material.dart';

import '/models/user.dart';
import '/utils/api.dart';

class AddUserScreen extends StatefulWidget {
	@override
	AddUserScreenState createState() => AddUserScreenState();
}

class AddUserScreenState extends State<AddUserScreen> {
    Future? futureUsers;

	final _formKey = GlobalKey<FormState>();
	final _nameController = TextEditingController();
	final _emailController = TextEditingController();
	final _phoneController = TextEditingController();
	final _websiteController = TextEditingController();

    void addUser(BuildContext context) {

        API().addUser(
            _nameController.text,
            _emailController.text,
            _phoneController.text,
            _websiteController.text
        );
        FutureBuilder(
            future: futureUsers, 
            builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                    Navigator.pushReplacementNamed(context, '/');
                }
                return Center(
                    child: CircularProgressIndicator()
                );
            });
    }

	@override
	void initState() {
		super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
			setState(() {
				futureUsers = API().addUser(
            _nameController.text,
            _emailController.text,
            _phoneController.text,
            _websiteController.text
        );
			});
		});
	}

	@override
	Widget build(BuildContext context) {

		Widget txtName = TextFormField(
			decoration: InputDecoration(labelText: 'Name'),
			keyboardType: TextInputType.text,
			controller: _nameController,
			validator: (value) {
				return (value != null && value.isNotEmpty) ? null  : 
				'Complete Name is required.';
			}
		);

		Widget txtEmail = TextFormField(
			decoration: InputDecoration(labelText: 'Email'),
			keyboardType: TextInputType.emailAddress,
			controller: _emailController,
			validator: (value) {
				return (value != null && value.isNotEmpty) ? null  : 
				'Email Address is required.';
			}
		);

		Widget txtPhone = TextFormField(
			decoration: InputDecoration(labelText: 'Phone'),
			keyboardType: TextInputType.number,
			controller: _phoneController,
			validator: (value) {
				return (value != null && value.isNotEmpty) ? null  : 
				'Phone Number is required.';
			}
		);	

		Widget txtWebsite = TextFormField(
			decoration: InputDecoration(labelText: 'Website'),
			keyboardType: TextInputType.text,
			controller: _websiteController,
			validator: (value) {
				return (value != null && value.isNotEmpty) ? null  : 
				'Website Link is required.';
			}
		);

		Widget btnSubmit = Container(
			width: double.infinity,
			margin: EdgeInsets.only(top:8.0),
			child: ElevatedButton(
				child: Text('Submit'),
				onPressed: () {
                    addUser(context);
				}
			)
		);

		Widget formAddUser = Form(
			key: _formKey,
			child: Column(
				children: [
					txtName,
					txtEmail,
					txtPhone,
					txtWebsite,
					btnSubmit
				]
			)
		);

		return Scaffold(
			appBar: AppBar(
                title: Text('Add User')),
			body: Container(
				width: double.infinity,
				padding: EdgeInsets.all(16.0),
				child: formAddUser
			),
		);	
	}	
}

	

import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
	Future getUsers() async {
		try {
			String url = 'https://jsonplaceholder.typicode.com/users';
			var response = await Dio().get(url);
			List<User> users = [];

			for (var user in response.data) {
				// The 'user' here is of Map<String, dynamic> type.
				// The 'user' map is converted into a User object.
				// After conversion, the object is added to the list.
				users.add(User.fromJson(user));
			}

			return users;
		} catch (exception) {
			throw exception;
		}
	}

    Future addUser(String name, String email, String phone, String website) async {
        try {
            final String url = 'https://jsonplaceholder.typicode.com/users';

            var response = await Dio().post(url, data: {
               "name": name,
               "email": email,
               "phone": phone,
               "website": website 
            });
            print(response.data);
            return response.data;
        } catch (exception) {
            throw exception;
        }
    }

    Future editUser(String name, String email, String phone, String website) async {
        try {
            final String url = 'https://jsonplaceholder.typicode.com/users/x';

            var response = await Dio().put(url, data: {
                "name": name,
                "email": email,
                "phone": phone,
                "website": website
            });

            return response.data;
        } catch (e) {
            throw e;
        }

    }
}